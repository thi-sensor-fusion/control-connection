#include <thi-sensor-fusion/control-connection/control_connection.hpp>

#include <unistd.h>
#include <sys/socket.h>
#include <sys/eventfd.h>

#include <labrat/cpp-utils/debug.hpp>

namespace thi_sensor_fusion {
  
ControlConnection::ControlConnection(const std::string host, const uint16_t port, const std::string client_host, const uint16_t client_port, const Callback callback) {
  
  address_client_any = false;
  this->callback = callback;
  
  // Convert the passed server address parameters to a socket address type.
  address_server.sin_family = AF_INET;
  address_server.sin_port = htons(port);
  if ( inet_pton(AF_INET, host.c_str(), &address_server.sin_addr) < 0 ) {
      perror("bind failed");
      return;
  }

  // Convert the passed client address parameters to a socket address type.
  address_client.sin_family = AF_INET;
  address_client.sin_port = htons(client_port);
  if ( inet_pton(AF_INET, client_host.c_str(), &address_client.sin_addr) < 0 ) {
      perror("bind failed");
      return;
  }

  if (address_client.sin_addr.s_addr == INADDR_ANY) {
    address_client_any = true;
  }

  // Open the udp socket.
  if ((socket_file_descriptor = socket(AF_INET, SOCK_DGRAM, 0)) < 0 ) {
      perror("socket creation failed");
      exit(EXIT_FAILURE);
  }

  if ( bind(socket_file_descriptor, (const struct sockaddr *)&address_server, sizeof(address_server)) < 0 ) {
      perror("bind failed");
      exit(EXIT_FAILURE);
  }



  epoll_file_descriptor = epoll_create1(0);
  if (epoll_file_descriptor == -1) {
    perror("Failed to create epoll fd");
  }
  
  // Create a event file descriptor which is used to stop the epoll thread.
  if ((stop_event_file_descriptor = eventfd(0, 0)) < 0 ) {
      perror("stop event creation failed");
      exit(EXIT_FAILURE);
  }

  struct epoll_event stop_event;
  stop_event.events = EPOLLIN;
  stop_event.data.ptr = &stop_event_file_descriptor;
  if (epoll_ctl(epoll_file_descriptor, EPOLL_CTL_ADD, stop_event_file_descriptor, &stop_event)) {
    close(epoll_file_descriptor);
    perror("Failed to add stop event to epoll fd");
  }

  // Specify the event.
  event_listen.events = EPOLLIN;
  event_listen.data.ptr = &socket_file_descriptor;

  // Add the listen event to the epoll fd. If an error occures the epoll fd is
  // closed first and then an error is thrown.
  if (epoll_ctl(epoll_file_descriptor, EPOLL_CTL_ADD, socket_file_descriptor, &event_listen)) {
    close(epoll_file_descriptor);
    perror("Failed to add fd to epoll fd");
  }

  thread = std::thread(listen, std::ref(*this));
}

ControlConnection::~ControlConnection() {
  uint64_t stop_event_value = 1;
  write(stop_event_file_descriptor, &stop_event_value, sizeof(stop_event_value));
  thread.join();
  close(socket_file_descriptor);
  close(epoll_file_descriptor);
}

void ControlConnection::send(const SendPacket &packet) {

  ++statistics.send;

  uint8_t buffer[sizeof(SendPacket) + 6];

  *(uint16_t *)(&buffer[0]) = htons(packet.camera_yaw);
  *(uint16_t *)(&buffer[2]) = htons(packet.camera_pitch);
  *(uint16_t *)(&buffer[4]) = htons(packet.camera_zoom);
  *(uint16_t *)(&buffer[6]) = htons(packet.camera_focus);
  *(uint16_t *)(&buffer[8]) = htons(packet.aircraft_yaw);
  *(uint16_t *)(&buffer[10]) = htons(packet.aircraft_pitch);
  *(uint16_t *)(&buffer[12]) = htons(packet.aircraft_roll);
  
  *(uint16_t *)(&buffer[14]) = 0x55;
  *(uint16_t *)(&buffer[16]) = htons(statistics.send);
  *(uint16_t *)(&buffer[18]) = htons(statistics.received);

  sendto(socket_file_descriptor, buffer, sizeof(buffer) + 6, 0, (const struct sockaddr *)&address_client, sizeof(address_client));
}
  
void ControlConnection::listen(ControlConnection &self) {
  uint8_t buffer[1000];
  ReceivePacket packet;
  sockaddr client_address;
  socklen_t client_address_length = sizeof(client_address);
  
  while (true) {
    static const uint16_t event_array_size = 2;
    // Event array to store triggered epoll events.
    std::array<struct epoll_event, event_array_size> event_array;
    int32_t number_events = epoll_wait(self.epoll_file_descriptor, event_array.data(), 1, -1);
        
    // Epoll returned an error. No data is processed.
    if (number_events == -1) {
      DEBUG_DEBUG("epoll returned -1: %d %s\n", errno, strerror(errno));
      continue;
    }
    
    // If result is 0, no events occured. As a result read() does not need
    // to be called.
    if (number_events == 0) {
      continue;
    }

    for (uint32_t i = 0; i < (uint32_t)number_events; ++i) {
      // If the stop event file descriptor caused a read event the epoll thread
      // returns.
      if (event_array[i].data.ptr == &self.stop_event_file_descriptor) {
        return;
      }

    
      if (event_array[i].data.ptr == &self.socket_file_descriptor) {
        ssize_t received = recvfrom(self.socket_file_descriptor, buffer, sizeof(buffer), 0, &client_address, &client_address_length);

        if (received <= -1 && self.socket_file_descriptor == 0) {
          return;
        }

        if (received < (ssize_t)sizeof(ReceivePacket)) {
          ++self.statistics.rejected_format;
        }

        ++self.statistics.received;

        packet.camera_yaw = ntohs(*(uint16_t *)(&buffer[0]));
        packet.camera_pitch = ntohs(*(uint16_t *)(&buffer[2]));
        packet.camera_zoom = ntohs(*(uint16_t *)(&buffer[4]));
        packet.camera_focus = ntohs(*(uint16_t *)(&buffer[6]));

        if (self.address_client_any) {
          self.address_client = *(sockaddr_in *)&client_address;
        } else if (self.address_client.sin_addr.s_addr != (*(sockaddr_in *)&client_address).sin_addr.s_addr || self.address_client.sin_port != (*(sockaddr_in *)&client_address).sin_port) {
          ++self.statistics.rejected_address;
          continue;
        }

        if (self.callback != nullptr) {
          self.callback(packet);
        }

      }
    }
  }
}

}
