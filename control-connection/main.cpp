#include <iostream>
#include <vector>
#include <chrono>
#include <cstdint>
#include <unistd.h>

#include <thi-sensor-fusion/control-connection/control_connection.hpp>

thi_sensor_fusion::ControlConnection *connection;

void callback(thi_sensor_fusion::ControlConnection::ReceivePacket &packet) {
  printf("cy: %d, cp: %d, cz: %d, cf: %d\n", packet.camera_yaw, packet.camera_pitch, packet.camera_zoom, packet.camera_focus);

  thi_sensor_fusion::ControlConnection::SendPacket pa{
    .camera_yaw = packet.camera_yaw,
    .camera_pitch = packet.camera_pitch,
    .camera_zoom = packet.camera_zoom,
    .camera_focus = packet.camera_focus,
    .aircraft_yaw = static_cast<uint16_t>(packet.camera_yaw / 2),
    .aircraft_pitch = static_cast<uint16_t>(packet.camera_pitch / 2 + 0xffff / 4),
    .aircraft_roll = static_cast<uint16_t>(packet.camera_yaw / 2 + 0xffff / 2),
  };

  connection->send(pa);
}

int main(int argc, char **argv) {

  (void)argc;
  (void)argv;

  connection = new thi_sensor_fusion::ControlConnection("0.0.0.0", 50042, "0.0.0.0", 50043, callback);

  sleep(300);

  delete connection;

  return 0;
}
