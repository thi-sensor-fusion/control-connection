#pragma once

#include <thread>
#include <sys/epoll.h>
#include <arpa/inet.h>

namespace thi_sensor_fusion {
    
  class ControlConnection {
  public:
    struct SendPacket {
      uint16_t camera_yaw;
      uint16_t camera_pitch;
      uint16_t camera_zoom;
      uint16_t camera_focus;
      uint16_t aircraft_yaw;
      uint16_t aircraft_pitch;
      uint16_t aircraft_roll;
    };

    struct ReceivePacket {
      uint16_t camera_yaw;
      uint16_t camera_pitch;
      uint16_t camera_zoom;
      uint16_t camera_focus;
    };

    struct Statistics {
      uint16_t send;
      uint16_t received;
      uint16_t rejected_address;
      uint16_t rejected_format;
    };

    using Callback = void (*)(ReceivePacket &packet);

    ControlConnection(const std::string host, const uint16_t port, const std::string client_host, const uint16_t client_port, const Callback callback);
    ~ControlConnection();

    void send(const SendPacket &packet);

    static void listen(ControlConnection &self);

  private:
    std::thread thread;
    int socket_file_descriptor;
    int epoll_file_descriptor;
    int stop_event_file_descriptor;

    struct epoll_event event_listen;
    sockaddr_in address_server;
    sockaddr_in address_client;
    bool address_client_any;
    Callback callback;
    
    Statistics statistics;
  };
    
} // namespace thi_sensor_fusion

